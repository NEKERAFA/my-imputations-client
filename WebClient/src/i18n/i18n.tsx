import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import app_es from './locales/es/App.json';

i18n.use(initReactI18next).init({
    lng: 'es',
    resources: {
        es: {
            App: app_es
        }
    }
});

export default i18n;