import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Typography from '@material-ui/core/Typography';
import Toolbar from '@material-ui/core/Toolbar';
import { useTranslation } from 'react-i18next';
import './App.css';

import Imputations from './imputations/Imputations';

const App = () => {
  const { t } = useTranslation('App');

  return (
    <BrowserRouter>
      <CssBaseline />
      <AppBar color="primary" position="static">
        <Toolbar>
          <Typography variant="h6">
            {t('My Imputations')}
          </Typography>
        </Toolbar>
      </AppBar>
      <Switch>
        <Route path="/">
          <Imputations />
        </Route>
      </Switch>
    </BrowserRouter>
  );
}

export default App;
